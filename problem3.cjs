const data = require('./drills_data.cjs');

const modelSort = ( objectData => {
    if(!Array.isArray(objectData) && !objectData.length){
        return([]);
    } else {
        const nameData = objectData
            .reduce((current, next) => {
                current.push(next['car_model']);
                // Never forget line 11 since you are retrieving data and need to return
                return current;         
            },[])
            // Nice chain linking
            .sort((firstWord,secondWord) => {
                firstWord = firstWord.toLowerCase();
                secondWord = secondWord.toLowerCase();
                if(firstWord > secondWord) {
                    return 1;
                } else if (firstWord < secondWord) {
                    return -1;
                }
                return 0;
            })
        return nameData;
    }
});

const final = modelSort(data);
console.log(final);

module.exports = modelSort;