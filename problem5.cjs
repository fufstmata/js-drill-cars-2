const data = require('./drills_data.cjs');
const yearFunction = require('./problem4.cjs');

const yearData = yearFunction(data);

const sortYears = (objectData) => {
    if(!Array.isArray(objectData)){
        return(0);
    } else {
        const validYear = objectData.filter(year => {
        return(year > 2000);
    }) 
        return(validYear.length);
    }
}

const count = sortYears(yearData);
console.log(count);

module.exports = sortYears;