const data = require('./drills_data.cjs');

const yearRetrieve = ( objectData => {
    if(!Array.isArray(objectData) || !objectData.length) {
        return [];
    } else {
        const yearData = objectData.reduce ((current, next) => {
            current.push(next['car_year']);
            // Don't forget line 10
            return current;
        },[])
        return yearData;
    }
})

const final = yearRetrieve(data);
console.log(final);

module.exports = yearRetrieve;