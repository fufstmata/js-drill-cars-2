const data = require('./drills_data.cjs');


// Dynamic nature achieved
// Filter in line 10 is neat
const locate = ( (objectData,idNumber) => {
    if(!Array.isArray(objectData) || !objectData.length || !Number.isFinite(idNumber)) {
        return([]);
    } else {
        const relevant = objectData.filter( current => {
            return current.id === idNumber;
        })
        return(relevant[0]);
    }
})


const final = locate(data,33);
console.log(`Car 33 is a ${final['car_year']} ${final['car_make']} ${final['car_model']}`);
module.exports = locate;
