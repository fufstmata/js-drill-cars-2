const data = require('./drills_data.cjs');

const Sauber = objectData => {
    if(!Array.isArray(objectData) || !objectData.length) {
        return([]);
    } else {
        const carData = objectData.reduce ((current , next) => {
            if(next['car_make'] === 'BMW' || next['car_make'] === 'Audi') {
                current.push(next);
            }
            return current;
        },[]);
        return carData;
    }
}

const final = Sauber(data);
console.log(final);

module.exports = Sauber;