// Very neat setup for testing (granted it is still primitive).
// Good for showcasing how the general layout will be with 'assert'

const test_func  = require('../problem1.cjs');
const data = require('../drills_data.cjs');
const assert = require('assert');

const test_data = test_func(data,33);
const expected = { id: 33, car_make: 'Jeep', car_model: 'Wrangler', car_year: 2011 };

// Refer to notes for difference between this and strictEqual
assert.deepStrictEqual(test_data,expected);
