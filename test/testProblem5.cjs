// Forgot about the need for exporting function for P4 as well
const year_test = require('../problem4.cjs');
const sort_test  = require('../problem5.cjs');
const data = require('../drills_data.cjs');
const assert = require('assert');

const year_data = year_test(data);
const test_data = sort_test(year_data);

const expected = 21

assert.strictEqual(test_data,expected);
