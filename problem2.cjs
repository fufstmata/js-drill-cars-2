const data = require('./drills_data.cjs');


// Not happy with this approach, since i'm again relying on filter
const detectLast = ( objectData => {
    if(Array.isArray(objectData) && objectData.length){
        const maxL = objectData.length;
        const last = objectData.filter( (current,indexValue) => {
            if(indexValue == maxL - 1) {
                return current;
            }
        })
        return(last[0]);
    } else {
        return([]);
    }
    
})

const final = detectLast(data);
console.log(`Last car is ${final['car_make']} ${final['car_model']}`);
module.exports = detectLast;